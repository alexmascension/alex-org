import os

root = "C:/Users/alexm/Dropbox/Alex-org/Emacs-conf/"

autocomplete_file = root + "auto-complete"
snippets_dir = root + "emacs.d/snippets/"

list_new_words = []

for ROOT, DIRS, FILES in os.walk(snippets_dir):
    for file in FILES:
        with open(ROOT+'/'+file, 'r') as f:
            lines = f.readlines()
            key = [i for i in lines if i[:6] == "# key:"][0]
            key = key[6:].strip(' ').strip('\n')
            list_new_words.append(key)

with open(autocomplete_file, 'r') as au:
    list_existing_words = au.readlines()
    list_existing_words = [i.strip('\n') for i in list_existing_words]

final_list = sorted(list(dict.fromkeys(list_new_words + list_existing_words)))

with open(autocomplete_file, 'w') as au:
    au.write('\n'.join(final_list))
