;; Set time stamps on org-mode to DAY/MONTH/YEAR
(setq-default org-display-custom-times t)
(setq org-time-stamp-custom-formats '("<%e-%b-%Y %a>" . "<%e-%b-%Y %a %H:%M>"))

;; Set the weekday to English
(setq system-time-locale "C")

;; Add CLOSED time
(setq org-log-done 'time)

;; Start calendar on Monday
(setq calendar-week-start-day 1)

;; Add sequence of TODO->DONE
(setq org-todo-keywords
      '((sequence "NEXT(n)" "TODO(t)" "HIGH(h)" "MID(m)" "LOW(l)" "CHECK(k)" "RECOVERABLE(r)" "FUTURE(f)" "DELEGATED(e)" "|" "DONE(d)" "CANCELLED(c)")))

