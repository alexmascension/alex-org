(setq symbol-alist '(
    ;; GREEK LETTERS
    ( "Alpha"  .   #x00391)( "Beta"  .   #x00392)( "Gamma"  .   #x00393)( "Delta"  .   #x00394)( "Epsilon"  .   #x00395)
    ( "Zeta"  .   #x00396)( "Eta"  .   #x00397)( "Theta"  .   #x00398)( "Iota"  .   #x00399)( "Kappa"  .   #x0039A)
    ( "Lambda"  .   #x0039B)( "Mu"  .   #x0039C)( "Nu"  .   #x0039D)( "Xi"  .   #x0039E)( "Omicron"  .   #x0039F)
    ( "Pi"  .   #x003A0)( "Rho"  .   #x003A1)( "Sigma"  .   #x003A3)( "Tau"  .   #x003A4)( "Upsilon"  .   #x003A5)
    ( "Phi"  .   #x003A6)( "Chi"  .   #x003A7)( "Psi"  .   #x003A8)( "Omega"  .   #x003A9)( "ohm"  .   #x003A9)
    ( "alpha"  .   #x003B1)( "beta"  .   #x003B2)( "gamma"  .   #x003B3)( "delta"  .   #x003B4)( "epsi"  .   #x003B5)
    ( "epsilon"  .   #x003B5)( "zeta"  .   #x003B6)( "eta"  .   #x003B7)( "theta"  .   #x003B8)( "iota"  .   #x003B9)
    ( "kappa"  .   #x003BA)( "lambda"  .   #x003BB)( "mu"  .   #x003BC)( "nu"  .   #x003BD)( "xi"  .   #x003BE)( "omicron"  .   #x003BF)
    ( "pi"  .   #x003C0)( "rho"  .   #x003C1)( "sigmav"  .   #x003C2)( "varsigma"  .   #x003C2)( "sigmaf"  .   #x003C2)
    ( "sigma"  .   #x003C3)( "tau"  .   #x003C4)( "upsi"  .   #x003C5)( "upsilon"  .   #x003C5)( "phi"  .   #x003C6)
    ( "chi"  .   #x003C7)( "psi"  .   #x003C8)( "omega"  .   #x003C9)( "thetav"  .   #x003D1)( "vartheta"  .   #x003D1)
    
    ;; LATEX INSIDE EQUATIONS
    ( "\\Alpha"  .   #x00391)( "\\Beta"  .   #x00392)( "\\Gamma"  .   #x00393)( "\\Delta"  .   #x00394)( "\\Epsilon"  .   #x00395)
    ( "\\Zeta"  .   #x00396)( "\\Eta"  .   #x00397)( "\\Theta"  .   #x00398)( "\\Iota"  .   #x00399)( "\\Kappa"  .   #x0039A)
    ( "\\Lambda"  .   #x0039B)( "\\Mu"  .   #x0039C)( "\\Nu"  .   #x0039D)( "\\Xi"  .   #x0039E)( "\\Omicron"  .   #x0039F)
    ( "\\Pi"  .   #x003A0)( "\\Rho"  .   #x003A1)( "\\Sigma"  .   #x003A3)( "\\Tau"  .   #x003A4)( "\\Upsilon"  .   #x003A5)
    ( "\\Phi"  .   #x003A6)( "\\Chi"  .   #x003A7)( "\\Psi"  .   #x003A8)( "\\Omega"  .   #x003A9)( "\\ohm"  .   #x003A9)
    ( "\\alpha"  .   #x003B1)( "\\beta"  .   #x003B2)( "\\gamma"  .   #x003B3)( "\\delta"  .   #x003B4)( "\\epsi"  .   #x003B5)
    ( "\\epsilon"  .   #x003B5)( "\\zeta"  .   #x003B6)( "\\eta"  .   #x003B7)( "\\theta"  .   #x003B8)( "\\iota"  .   #x003B9)
    ( "\\kappa"  .   #x003BA)( "\\lambda"  .   #x003BB)( "\\mu"  .   #x003BC)( "\\nu"  .   #x003BD)( "\\xi"  .   #x003BE)( "\\omicron"  .   #x003BF)
    ( "\\pi"  .   #x003C0)( "\\rho"  .   #x003C1)( "\\sigmav"  .   #x003C2)( "\\varsigma"  .   #x003C2)( "\\sigmaf"  .   #x003C2)
    ( "\\sigma"  .   #x003C3)( "\\tau"  .   #x003C4)( "\\upsi"  .   #x003C5)( "\\upsilon"  .   #x003C5)( "\\phi"  .   #x003C6)
    ( "\\chi"  .   #x003C7)( "\\psi"  .   #x003C8)( "\\omega"  .   #x003C9)( "\\thetav"  .   #x003D1)( "\\vartheta"  .   #x003D1)

    ;; COMMON SYMBOLS
    (">=" . "≥") ("\\ge" . "≥") ("<=" . "≤") ("\\le" . "≤") ("<-" . "←") ("->" . "→") ("more or less" . "±")
   
    ;; MATH SYMBOLS
    ("\\forall" . #x2200)("\\empty" . #x2205)("\\nabla" . #x2207)("\\in" . #x2208)("\\notin" . #x2209)("\\not\\in" . #x2209)("\\to" . #x2192)
    ("\\prod" . #x220F)("\\sum" . #x2211)("\\pm" . #x2214)("\\mp" . #x2213)("\\propto" . #x221D)("\\infty" . #x221E)
    ("\\prec" . #x227A)("\\succ" . #x227B)("\\preceq" . #x227C)("\\succeq" . #x227D)
    ("\\vee" . #x2227)("\\wedge" . #x2228)("\\cap" . #x2229)("\\cup" . #x222A)("\\sum" . #x003A3)
    ("\\bigvee" . #x22C0)("\\bigwedge" . #x22C1)("\\bigcap" . #x22C2)("\\bigcup" . #x22C3)
    ("\\int" . #x222B)("\\iint" . #x222C)("\\iiint" . #x222D)("\\oint" . #x222E)("\\oiint" . #x222F)("\\oiiint" . #x2230)
    ("\\therefore" . #x2234)("\\because" . #x2235)("\\sim" . #x223C)("\\simeq" . #x2243)("\\approx" . #x2249)("\\equiv" . #x2261)
    ("\\ll" . #x226A)("\\gg" . #x226B)("\\nleq" . #x2270)("\\ngeq" . #x2271)("\\subset" . #x2282)("\\supset" . #x2283)
    ("\\nsubset" . #x2284)("\\nsuperset" . #x2286)("\\subseteq" . #x2287)("\\superseteq" . #x2288)("\\nsubseteq" . #x2289)("\\nsuperseteq" . #x228A)
    ("\\ni" . #x220B)("\\neq" . #x2260)("\\oplus" . #x2295)("\\bigoplus" . #x2A01)("\\otimes" . #x2297)("\\bigotimes" . #x2A02)
    ("\\times" . #x00D7)("\\cdot" . #x00B7)("\\cdots" . #x2026)("\\vdots" . #x22EE)("\\ddots" . #x22F1)
    ("\\dagger" . #x2020)("\\Vert" . #x2016)("\\vert" . "|")

    ("\\leftarrow" . #x2190)("\\Leftarrow" . #x21D0)("\\rightarrow" . #x2192)("\\Rightarrow" . #x21D2)("\\rightleftarrow" . #x21C4)
    ("\\implies" . #x21D2)("\\impliedby" . #x21D0)("\\iff" . #x21D4)("\\leftrightarrow" . #x2194)("\\Leftrightarrow" . #x21D4)
    ("\\mapsto" . #x21A6)("\\uparrow" . #x2191)("\\Uparrow" . #x21D1)("\\downarrow" . #x2193)("\\Downarrow" . #x21D3)
    ("\\neg" . #x00AC)("\\hbar" . #x210F)("\\lceil" . #x2308)("\\rceil" . #x2309)("\\lfloor" . #x230A)("\\rfloor" . #x230B)
    ("\\triangle" . #x25B5)("\\square" . #x25A1)("\\angle" . #x2220)("\\measuredangle" . #x2221)("\\perp" . #x22A5)
 
    ;; MATH LETTERS (FRAC; CAL; BB)
    ("\\mathbb{A}"  .  #x1D538) ("\\mathbb{B}"  .  #x1D539) ("\\mathbb{C}"  .  #x2102) ("\\mathbb{D}"  .  #x1D53B) ("\\mathbb{E}"  .  #x1D53C) ("\\mathbb{F}"  .  #x1D53D) 
    ("\\mathbb{G}"  .  #x1D53E) ("\\mathbb{H}"  .  #x210D) ("\\mathbb{I}"  .  #x1D540) ("\\mathbb{J}"  .  #x1D541) ("\\mathbb{K}"  .  #x1D542) 
    ("\\mathbb{L}"  .  #x1D543) ("\\mathbb{M}"  .  #x1D544) ("\\mathbb{N}"  .  #x2115) ("\\mathbb{O}"  .  #x1D546) ("\\mathbb{P}"  .  #x2119) 
    ("\\mathbb{Q}"  .  #x211A) ("\\mathbb{R}"  .  #x211D) ("\\mathbb{S}"  .  #x1D54A) ("\\mathbb{T}"  .  #x1D54B) ("\\mathbb{U}"  .  #x1D54C) 
    ("\\mathbb{V}"  .  #x1D54D) ("\\mathbb{W}"  .  #x1D54E) ("\\mathbb{X}"  .  #x1D54F) ("\\mathbb{Y}"  .  #x1D550) ("\\mathbb{Z}"  .  #x2124) 
    ("\\mathbb{a}"  .  #x1D552) ("\\mathbb{b}"  .  #x1D553) ("\\mathbb{c}"  .  #x1D554) ("\\mathbb{d}"  .  #x1D555) ("\\mathbb{e}"  .  #x1D556) 
    ("\\mathbb{f}"  .  #x1D557) ("\\mathbb{g}"  .  #x1D558) ("\\mathbb{h}"  .  #x1D559) ("\\mathbb{i}"  .  #x1D55A) ("\\mathbb{j}"  .  #x1D55B) 
    ("\\mathbb{k}"  .  #x1D55C) ("\\mathbb{l}"  .  #x1D55D) ("\\mathbb{m}"  .  #x1D55E) ("\\mathbb{n}"  .  #x1D55F) ("\\mathbb{o}"  .  #x1D560) 
    ("\\mathbb{p}"  .  #x1D561) ("\\mathbb{q}"  .  #x1D562) ("\\mathbb{r}"  .  #x1D563) ("\\mathbb{s}"  .  #x1D564) ("\\mathbb{t}"  .  #x1D565) 
    ("\\mathbb{u}"  .  #x1D566) ("\\mathbb{v}"  .  #x1D567) ("\\mathbb{w}"  .  #x1D568) ("\\mathbb{x}"  .  #x1D569) ("\\mathbb{y}"  .  #x1D56A) 
    ("\\mathbb{z}"  .  #x1D56B)
    ("\\N" . #x2115) ("\\Z" . #x2124) ("\\Q" . #x211A) ("\\R" . #x211D) ("\\C" . #x2102)

    ("\\mathfrak{A}"  .  #x1D504) ("\\mathfrak{B}"  .  #x1D505) ("\\mathfrak{C}"  .  #x212D) ("\\mathfrak{D}"  .  #x1D507) 
    ("\\mathfrak{E}"  .  #x1D508) ("\\mathfrak{F}"  .  #x1D509) ("\\mathfrak{G}"  .  #x1D50A) ("\\mathfrak{H}"  .  #x210C) ("\\mathfrak{I}"  .  #x2111) 
    ("\\mathfrak{J}"  .  #x1D50D) ("\\mathfrak{K}"  .  #x1D50E) ("\\mathfrak{L}"  .  #x1D50F) ("\\mathfrak{M}"  .  #x1D510) ("\\mathfrak{N}"  .  #x1D511) 
    ("\\mathfrak{O}"  .  #x1D512) ("\\mathfrak{P}"  .  #x1D513) ("\\mathfrak{Q}"  .  #x1D514) ("\\mathfrak{R}"  .  #x211C) ("\\mathfrak{S}"  .  #x1D516) 
    ("\\mathfrak{T}"  .  #x1D517) ("\\mathfrak{U}"  .  #x1D518) ("\\mathfrak{V}"  .  #x1D519) ("\\mathfrak{W}"  .  #x1D51A) ("\\mathfrak{X}"  .  #x1D51B) 
    ("\\mathfrak{Y}"  .  #x1D51C) ("\\mathfrak{Z}"  .  #x2128) ("\\mathfrak{a}"  .  #x1D51E) ("\\mathfrak{b}"  .  #x1D51F) ("\\mathfrak{c}"  .  #x1D520) 
    ("\\mathfrak{d}"  .  #x1D521) ("\\mathfrak{e}"  .  #x1D522) ("\\mathfrak{f}"  .  #x1D523) ("\\mathfrak{g}"  .  #x1D524) ("\\mathfrak{h}"  .  #x1D525) 
    ("\\mathfrak{i}"  .  #x1D526) ("\\mathfrak{j}"  .  #x1D527) ("\\mathfrak{k}"  .  #x1D528) ("\\mathfrak{l}"  .  #x1D529) ("\\mathfrak{m}"  .  #x1D52A) 
    ("\\mathfrak{n}"  .  #x1D52B) ("\\mathfrak{o}"  .  #x1D52C) ("\\mathfrak{p}"  .  #x1D52D) ("\\mathfrak{q}"  .  #x1D52E) ("\\mathfrak{r}"  .  #x1D52F) 
    ("\\mathfrak{s}"  .  #x1D530) ("\\mathfrak{t}"  .  #x1D531) ("\\mathfrak{u}"  .  #x1D532) ("\\mathfrak{v}"  .  #x1D533) ("\\mathfrak{w}"  .  #x1D534) 
    ("\\mathfrak{x}"  .  #x1D535) ("\\mathfrak{y}"  .  #x1D536) ("\\mathfrak{z}"  .  #x1D537)
    ("\\Im" . #x2111) ("\\Re" . #x211C)

    ("\\mathcal{A}"  .  #x1D49C) ("\\mathcal{B}"  .  #x212C) 
    ("\\mathcal{C}"  .  #x1D49E) ("\\mathcal{D}"  .  #x1D49F) ("\\mathcal{E}"  .  #x2130) ("\\mathcal{F}"  .  #x2131) ("\\mathcal{G}"  .  #x1D4A2) 
    ("\\mathcal{H}"  .  #x210B) ("\\mathcal{I}"  .  #x2110) ("\\mathcal{J}"  .  #x1D4A5) ("\\mathcal{K}"  .  #x1D4A6) ("\\mathcal{L}"  .  #x2112) 
    ("\\mathcal{M}"  .  #x2133) ("\\mathcal{N}"  .  #x1D4A9) ("\\mathcal{O}"  .  #x1D4AA) ("\\mathcal{P}"  .  #x1D4AB) ("\\mathcal{Q}"  .  #x1D4AC) 
    ("\\mathcal{R}"  .  #x211B) ("\\mathcal{S}"  .  #x1D4AE) ("\\mathcal{T}"  .  #x1D4AF) ("\\mathcal{U}"  .  #x1D4B0) ("\\mathcal{V}"  .  #x1D4B1) 
    ("\\mathcal{W}"  .  #x1D4B2) ("\\mathcal{X}"  .  #x1D4B3) ("\\mathcal{Y}"  .  #x1D4B4) ("\\mathcal{Z}"  .  #x1D4B5) ("\\mathcal{a}"  .  #x1D4B6) 
    ("\\mathcal{b}"  .  #x1D4B7) ("\\mathcal{c}"  .  #x1D4B8) ("\\mathcal{d}"  .  #x1D4B9) ("\\mathcal{e}"  .  #x212F) ("\\mathcal{f}"  .  #x1D4BB) 
    ("\\mathcal{g}"  .  #x210A) ("\\mathcal{h}"  .  #x1D4BD) ("\\mathcal{i}"  .  #x1D4BE) ("\\mathcal{j}"  .  #x1D4BF) ("\\mathcal{k}"  .  #x1D4C0) 
    ("\\mathcal{l}"  .  #x1D4C1) ("\\mathcal{m}"  .  #x1D4C2) ("\\mathcal{n}"  .  #x1D4C3) ("\\mathcal{o}"  .  #x1D4C4) ("\\mathcal{p}"  .  #x1D4C5) 
    ("\\mathcal{q}"  .  #x1D4C6) ("\\mathcal{r}"  .  #x1D4C7) ("\\mathcal{s}"  .  #x1D4C8) ("\\mathcal{t}"  .  #x1D4C9) ("\\mathcal{u}"  .  #x1D4CA) 
    ("\\mathcal{v}"  .  #x1D4CB) ("\\mathcal{w}"  .  #x1D4CC) ("\\mathcal{x}"  .  #x1D4CD) ("\\mathcal{y}"  .  #x1D4CE) ("\\mathcal{z}"  .  #x1D4CF) 


    ;; PARENTHESIS/BRACES
    ("\\left("  .  "(")("\\right)"  .  ")")("\\left["  .  "[")("\\right]"  .  "]")("\\left\\{"  .  "\\{")("\\right\\}"  .  "\\}")
    ("\\left\\lceil"  .  #x250C)("\\right\\rceil"  .  #x2510)("\\left\\lfloor"  .  #x2514)("\\right\\rfloor"  .  #x2518)
    ("\\lceil"  .  #x250C)("\\rceil"  .  #x2510)("\\lfloor"  .  #x2514)("\\rfloor"  .  #x2518)
    ("\\left\\Vert"  .  #x2016)("\\right\\Vert"  .  #x2016)("\\left\\vert" . "|")("\\right\\vert" . "|")


    ;; HEBREW LETTERS
    ("\\aleph" . #x2135)("\\beth" . #x2136)("\\gmel" . #x2137)("\\daleth" . #x2138)

    ;; MEASURES
    ("angstrom" . "Å")("Angstrom" . "Å")("um" . "µm")("micron" . "µ")("micrometer"  .  "µm")

    ;; CHECK BOXES
    ("[ ]" .  ?◯)("[X]" .  ?✓)
))

(global-prettify-symbols-mode 1)
(setq prettify-symbols-unprettify-at-point 'right-edge)
(add-hook 'org-mode-hook (lambda ()   (mapc (lambda (pair) (push pair prettify-symbols-alist)) symbol-alist)))
(add-hook 'AUCTeX-mode-hook (lambda ()   (mapc (lambda (pair) (push pair prettify-symbols-alist)) symbol-alist)))
(add-hook 'TeX-mode-hook (lambda ()   (mapc (lambda (pair) (push pair prettify-symbols-alist)) symbol-alist)))
