;; Themes must be included in .emacs.d/themes

;; TRON
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")
(load-theme `tron t)