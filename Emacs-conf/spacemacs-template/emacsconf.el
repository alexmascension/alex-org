;; ___________________________________ RUN COMMANDS ________________________________________
;; Use calc and other languages to run code
(org-babel-do-load-languages
   'org-babel-load-languages
      '((calc . t)
        (python . t)))

;; ___________________________________ GLOBAL KEY BINDINGS __________________________________________
;; Add agenda launching keybindings
 ;; This is for key bindings to invoke agenda mode (see line-2)
 (global-set-key "\C-cl" 'org-store-link)
 (global-set-key "\C-ca" 'org-agenda)
 (global-set-key "\C-cc" 'org-capture)
 (global-set-key "\C-cb" 'org-iswitchb)
 (global-set-key "\C-cm" 'org-ref-helm-insert-cite-link)
 (global-set-key "\C-xg" 'magit-status)
 (global-set-key "\C-co" 'prettify-symbols-mode)

  ;; Give C-c ] back to org-agenda [although should not be necessary).
 (setq org-ref-insert-cite-key "C-c )")


 ;; To hide bolds, italic, etc. markings
 (setq org-hide-emphasis-markers t)

 ;; Key macro to render images
 (fset 'render-image
       "\C-c\C-x\C-v")
 (global-set-key "\C-ci" 'render-image)

 ;; Macro to indent whole paragraphs
 (fset 'indent-block
   "\234")

 (global-set-key "\C-cd" 'indent-block)


 ;;----------------------------------- FILL COLUMN ------------------------------------------------
  (global-set-key (kbd "C-c f") (lambda () (interactive) (fci-mode) (auto-fill-mode)))
  (setq fci-rule-width 2)
  (setq fci-rule-color "gray30")
  (setq fci-style 'rule)
