;; Set a unified command to download new metadada, sync from trello and sync to trello
(fset 'full-trello-cycle "\C-cou\C-u\C-cos\C-cos")
(global-set-key "\C-cu" 'full-trello-cycle)
(global-set-key "\C-ct" 'org-trello-mode)
