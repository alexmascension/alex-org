;; (add-to-list 'ac-user-dictionary-files auto-complete-file)
(require 'auto-complete)
(ac-config-default)
(define-key ac-mode-map (kbd "TAB") nil)
(define-key ac-completing-map (kbd "TAB") nil)
(define-key ac-completing-map [tab] nil)
(ac-set-trigger-key "RET")
(global-auto-complete-mode t)

;; allows auto-complete to work in all buffers
(defun auto-complete-mode-maybe ()
  "No maybe for you. Only AC!"
  (unless (minibufferp (current-buffer))
    (auto-complete-mode 1)))

(add-to-list 'ac-user-dictionary-files auto-complete-file)
