(setq org-capture-templates
      '(
        ("t" "General Todo")
        ("tt" "TODO" plain (file general-todos-file)
         "\n\n*** TODO %^{TEXTO} %T \n\n %?")
        ("tg" "TODO with TAGS" plain (file general-todos-file)
         "\n\n*** TODO %^{TEXTO} %T %^g \n\n %?")

        ("b" "Biodonostia Todo")
        ("bb" "TODO" plain (file biodonostia-todos-file)
         "\n\n* TODO %^{TEXTO} %T %^g \n\n %?")
        ("bg" "TODO with TAGS" plain (file biodonostia-todos-file)
         "\n\n* TODO %^{TEXTO} %T %^g \n\n %?")
        ))
