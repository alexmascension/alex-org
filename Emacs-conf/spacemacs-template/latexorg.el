;; Make formulas a bit bigger
(setq org-format-latex-options (plist-put org-format-latex-options :scale 1.25))
(fset 'render-latex-buffer-all
   "\C-u\C-u\C-c\C-x\C-l")
(fset 'render-latex-buffer
      "\C-c\C-x\C-l")
(fset 're-render-latex-buffer
     "\C-u\C-u\C-c\C-x\C-l\C-u\C-u\C-c\C-x\C-l" )
(global-set-key "\C-cq" 'render-latex-buffer)
(global-set-key "\C-cr" 'render-latex-buffer-all)
(global-set-key "\C-cs" 're-render-latex-buffer)


;; EXPORT LATEX AS C-c x
(fset 'export-latex
     "\C-c\C-elp" )
(global-set-key "\C-cx" 'export-latex)


;; FONT-SIZE
(setq org-format-latex-options (plist-put org-format-latex-options :scale 1.5))


;; LATEX CLASS EXTARTICLE
(with-eval-after-load 'ox-latex
(add-to-list 'org-latex-classes
          '("extarticle"
             "\\documentclass[a4paper, 11pt]{extarticle}
             [NO-DEFAULT-PACKAGES]
             [EXTRA]"
             ("\\section{%s}" . "\\section*{%s}")
             ("\\subsection{%s}" . "\\subsection*{%s}")
             ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
             ("\\paragraph{%s}" . "\\paragraph*{%s}")
             ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))))


;; fix color handling in org-preview-latex-fragment
(let ((dvipng--plist (alist-get 'dvipng org-preview-latex-process-alist)))
  (plist-put dvipng--plist :use-xcolor t)
  (plist-put dvipng--plist :image-converter '("dvipng -D %D -T tight -o %O %f")))

(if (eq islinux 0)
(setq doc-view-ghostscript-program "C:/gs/gs9.27/bin/gswin64.exe")
)