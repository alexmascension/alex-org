(defun my-org-latex-yas ()
  "Activate org and LaTeX yas expansion in org-mode buffers."
  (yas-minor-mode)
  (yas-activate-extra-mode 'latex-mode))

(add-hook 'org-mode-hook #'my-org-latex-yas)

(yas-global-mode)

(global-set-key "\C-cz" 'yas-new-snippet)


(setq yas-triggers-in-field t)
