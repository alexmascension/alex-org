(org-super-agenda-mode t)

(with-eval-after-load 'org (setq org-super-agenda-groups
'( ;; Each group has an implicit boolean OR operator between its selectors.
  (:name "NEXT" 
         :todo "NEXT"
         :order 2
         :face (:foreground "CornflowerBlue"))
  (:name "TODAY"
         :deadline today
         :scheduled today
         :order 3
         :face (:foreground "LimeGreen"))
  (:name "DEADLINED"
         :deadline past
         :scheduled past
         :order 5
         :face (:foreground "brown")) 
  (:name "NEAR DEADLINE"
         :deadline future
         :scheduled future
         :order 6
         :face (:foreground "PaleVioletRed")) 
  (:name "IMPORTANT"
               :priority "A"
               :todo "HIGH"
               :order 8)
  (:name "MID PRIORITY"
               :priority "B"
               :todo "MID"
               :order 10)
  (:name "LOW PRIORITY"
               :priority "C"
               :todo "LOW"
               :order 12)
  (:name "EMACS"
         :file-path "Tareas-emacs.org"
               :tag "emacs"
               :order 30)

  (:name "DELEGATED"
               :todo "DELEGATED"
               :order 100
               :face (:foreground "SlateGrey"))
  (:name "BA�L DE LOS RECUERDOS"
               :todo "RECOVERABLE"
               :todo "FUTURE"
               :order 101
               :face (:foreground "SlateGrey"))
         ;; After the last group, the agenda will display items that didn't
         ;; match any of these groups, with the default order position of 99
         )))


(load-library "find-lisp")
(with-eval-after-load 'org  (setq org-agenda-files (find-lisp-find-files orgdir "\.org$")))
