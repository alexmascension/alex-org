import os

root = "C:/Users/alexm/Dropbox/Alex-org/Emacs-conf/"

snippets_dir = root + "emacs.d/snippets/"

list_new_words = []

for ROOT, DIRS, FILES in os.walk(snippets_dir):
    for file in FILES:
        with open(ROOT + '/' + file, 'r') as f:
            text = f.read()

        if text.endswith('\n'):
            text = text[:-3] + text[-3:].replace('\n', '')

        with open(ROOT + '/' + file, 'w') as f:
            f.write(text)
