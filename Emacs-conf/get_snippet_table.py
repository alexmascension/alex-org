'''
This code produces a .org file with the description of each snippet. This file will be a table that will be rendered
to a pdf once is opened in emacs (C-c x). Each cell of the table has a title in bold (the name of the command), and the
action. The action will be a representation of the code as it is if key==name, else it will be the description from name.
'''

import os
import re

dir_snippets = "C:/Users/alexm/Dropbox/Alex-org/Emacs-conf/emacs.d/snippets/org-mode/"
file_out = "C:/Users/alexm/Dropbox/Alex-org/Emacs-conf/snippets_chuleta.org"
n_cols = 5
text_out = ""

list_files = sorted(os.listdir(dir_snippets))

text_out += """
#+TITLE: 
#+DATE:
#+LaTeX_CLASS: article
#+LaTeX_HEADER: \\usepackage[utf8]{inputenc}
#+LaTeX_HEADER: \\usepackage[left=1.5cm, right=1.5cm, bottom=2cm, top=2cm]{geometry}

#+LaTeX_HEADER: % Paquetes de matemáticas
#+LaTeX_HEADER: \\usepackage{amsmath, amsfonts, amssymb, commath}
#+LaTeX_HEADER: \\usepackage{xcolor, longtable}


#+LaTeX_HEADER: \\usepackage[T1]{fontenc}
#+LaTeX_HEADER: \\usepackage{newpxtext, newpxmath}

#+LaTeX_HEADER: \\usepackage{tikz}

#+LaTeX_HEADER: \\DeclareMathAlphabet{\\pazocal}{OMS}{zplm}{m}{n}
#+LaTeX_HEADER: \\let\\mathcal\\pazocal

#+LATEX: \\vspace{-4em}
#+OPTIONS: toc:nil ;;

Los objetos en \\textcolor{red}{rojo} son elementos que aparecen por defecto y que el usuario puede cambiar. 
Los elementos en \\textcolor{olive}{amarillo} son aquellos que por complejidad no se muestran en la hoja, y que tienen 
la explicación para entender lo que hacen. La explicación se hace en el archivo escribiendo #name: VVExplicacion.\n\n
"""

text_out += "\\begin{longtable}{%s}" % ('c'*n_cols)

counter = 1
c_i = 0
for file in list_files:
    file_in = open(dir_snippets + file, 'r').readlines()
    name_in = file_in[1].replace('\n', '').replace('# name:', '')
    key_in = file_in[2].replace('\n', '').replace('# key:', '')
    text_in = ''.join(file_in[4:])

    # We will change the way the names are considered to specific cases.
    if '__' in key_in:
        key_out = '\\_\\_'
    elif ('<' in key_in) or ('>' in key_in):
        key_out = "\\( %s \\)" %key_in
    elif '{' in key_in:
        key_out = key_in.replace('{', '\\{')
    else:
        key_out = key_in

    key_out = str(counter) + ' ' + key_out
    counter += 1
    if "VV" in name_in:
        name_out = "\\textcolor{olive}{%s}" %name_in.replace('VV', '')
        print(name_out)
    else:
        name_out = "\\( %s \\)" %name_in.replace(' ', '')

    if name_in.replace(' ', '') != key_in.replace(' ', ''):
        text_out += "\\begin{tabular}{ @ {}c @ {}} \\textbf{%s} \\\\ %s  \\end{tabular}" %(key_out, name_out)
    else:
        "We will have to do some filtering of $X or ${X: Y}"

        # 1) Remove 4 bars into 2, and change \} to }
        text_in = text_in.replace('\n', '').replace('\\\\', '\\')


        # 2) Transform ${X: f} -> f

        #There are some specific cases like in lim (${1:\infty}) or sum (${1:i=1}) which are easier to arrange manually
        if "${1:\\infty}" in text_in: # Lims and other elements
            text_in = text_in.replace("${1:\\infty}", "\\textcolor{red}{\\infty}")
        if "${1:i=1}" in text_in: # Sums and other elements
            text_in = text_in.replace("${1:i=1}", "\\textcolor{red}{i=1}")
        if "${1:1}" in text_in: # Compo elements
            text_in = text_in.replace("${1:1}", "1").replace("$1", "1")

        ocur = re.findall("\$\{\d+:\s*\w+\s*\}", text_in) # First we look for occurrences
        for o in ocur:
            try:
                # E.G. ${1:x} Hola $1    should be     x Hola x
                ocurx = re.search("\$\{\d+:\s*(.*?)\s*\}", o).group(1) # Now we transform the occurrence with the text
                ocurx_n = o.replace(' ', '').replace('{', '').replace('}', '').replace(':', '').replace(ocurx, '')
            except AttributeError:
                ocurx = ''  # apply your error handling
                ocurx_n = ''

            text_in = text_in.replace(o, "\\textcolor{red}{%s}" %ocurx)
            text_in = text_in.replace(ocurx_n, "\\textcolor{red}{%s}" %ocurx)

        # 3) Transform $1 -> x, $2 -> y, $3 -> z
        text_in = text_in.replace('$0', '').replace('$1', '\\textcolor{red}{x}').replace('$2', '\\textcolor{red}{y}').replace('$3', '\\textcolor{red}{z}')

        # 4) Determine whether to add or not an equation environment:
        iseq = True
        if text_in.__contains__("\\begin{matrix") or text_in.__contains__("\\begin{align") :
            iseq = True
        elif text_in.__contains__("\\begin{") or text_in.__contains__("\\begin{tikz"):
            iseq = False

        if iseq:
            text_out += "\\begin{tabular}{ @ {}c @ {}} \\textbf{%s} \\\\ \\( %s \\)  \\end{tabular}" %(key_out, text_in)
        else:
            text_out += "\\begin{tabular}{ @ {}c @ {}} \\textbf{%s} \\\\ %s  \\end{tabular}" %(key_out, text_in)
    c_i += 1

    if c_i == n_cols:
        text_out += ' \\\\ \\vspace{0.5em} \n'
        c_i = 0
    else:
        text_out += " & \n"


# Now we check out the counter of the table to end it if necessary
if c_i != n_cols:
    text_out += " & \n" * (n_cols - c_i - 1) + " \\\\ \\vspace{0.5em} \n"

text_out += "\\end{longtable}"

print(text_out)

with open(file_out, 'w') as fo:
    fo.write(text_out)